<?php

// Route::get('/home', function () {
//     // $users[] = Auth::user();
//     // $users[] = Auth::guard()->user();
//     // $users[] = Auth::guard('admin')->user();

//     //dd($users);

//     return view('admin.home');
// })->name('home');

Route::get('home', 'AdminAuth\HomeController@index')->name('home');

Route::post('/bank/delete-all', 'AdminAuth\BankController@postDeleteAll')->name('bank.delete.all');
Route::get('/bank/getData', 'AdminAuth\BankController@getData')->name('bank.getData');
Route::resource('bank', 'AdminAuth\BankController');

Route::post('/category/delete-all', 'AdminAuth\KategoriBarangController@postDeleteAll')->name('category.delete.all');
Route::get('/category/getData', 'AdminAuth\KategoriBarangController@getData')->name('category.getData');
Route::resource('category', 'AdminAuth\KategoriBarangController');

Route::post('/item/delete-all', 'AdminAuth\BarangController@postDeleteAll')->name('item.delete.all');
Route::get('/item/getData', 'AdminAuth\BarangController@getData')->name('item.getData');
Route::resource('item', 'AdminAuth\BarangController');

Route::post('/staff/delete-all', 'AdminAuth\PegawaiController@postDeleteAll')->name('staff.delete.all');
Route::get('/staff/getData', 'AdminAuth\PegawaiController@getData')->name('staff.getData');
Route::resource('staff', 'AdminAuth\PegawaiController');

Route::post('/customer/delete-all', 'AdminAuth\PelangganController@postDeleteAll')->name('customer.delete.all');
Route::get('/customer/getData', 'AdminAuth\PelangganController@getData')->name('customer.getData');
Route::resource('customer', 'AdminAuth\PelangganController');


Route::post('/itemStatus/delete-all', 'AdminAuth\StatusBarangController@postDeleteAll')->name('itemStatus.delete.all');
Route::get('/itemStatus/getData', 'AdminAuth\StatusBarangController@getData')->name('itemStatus.getData');
Route::resource('itemStatus', 'AdminAuth\StatusBarangController');

Route::get('/about', 'AdminAuth\AboutController@index')->name('about.index');
Route::post('/about', 'AdminAuth\AboutController@store')->name('about.store');

Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
Route::post('/profile', 'Admin\ProfileController@store')->name('profile.store');

Route::post('/transaction/delete-all', 'AdminAuth\TransaksiController@postDeleteAll')->name('transaction.delete.all');
Route::get('/transaction/getData', 'AdminAuth\TransaksiController@getData')->name('transaction.getData');
Route::get('/transaction/modal/{id?}', 'AdminAuth\TransaksiController@modal')->name('transaction.modal');
Route::get('transaction', 'AdminAuth\TransaksiController@index')->name('transaction.index');
Route::get('transaction/{id}/edit', 'AdminAuth\TransaksiController@edit')->name('transaction.edit');
Route::delete('transaction/{id}', 'AdminAuth\TransaksiController@destroy')->name('transaction.destroy');
Route::post('transaction/{id}', 'AdminAuth\TransaksiController@store')->name('transaction.store');

Route::post('/confirmation/delete-all', ['as' => 'confirmation.delete.all', 'uses' => 'AdminAuth\KonfirmasiController@postDeleteAll']);
Route::get('/confirmation/getData', 'AdminAuth\KonfirmasiController@getData')->name('confirmation.getData');
Route::get('confirmation/{id}/edit', ['as' => 'confirmation.edit', 'uses' => 'AdminAuth\KonfirmasiController@edit', 'middleware' => ['acl:index_confirmation']]);
Route::put('confirmation/{id}', 'AdminAuth\KonfirmasiController@update')->name('confirmation.update');
// Route::get('confirmation', ['as' => 'confirmation.index', 'uses' => 'AdminAuth\KonfirmasiController@index']);
Route::resource('confirmation', 'AdminAuth\KonfirmasiController');

Route::post('/access/delete-all', ['as' => 'access.delete.all', 'uses' => 'AdminAuth\HakAksesController@postDeleteAll']);
Route::get('/access/getData', 'AdminAuth\HakAksesController@getData')->name('access.getData');
Route::get('access/{id}/edit', ['as' => 'access.edit', 'uses' => 'AdminAuth\HakAksesController@edit']);
Route::resource('access', 'AdminAuth\HakAksesController');

// Route::post('/status/delete-all', 'AdminAuth\StatusController@postDeleteAll')->name('status.delete.all');
// Route::get('/status/getData', 'AdminAuth\StatusController@getData')->name('status.getData');
// Route::resource('status', 'AdminAuth\StatusController');