$(function() {
    "use strict";
    $('.delete-all').confirmModal({
      confirmCallback: function(){
        $('.confirm').submit();
      }
    });

    $(document).on('click','.delete',function(e) {
      e.preventDefault();
      $('.delete').confirmModal({confirmAutoOpen : true});
    });
})