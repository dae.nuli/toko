<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaksi extends Model
{
    protected $table = 'transaksi';

    public function barang()
    {
        return $this->belongsTo('App\Model\Barang','id_barang');
    }

    public function customer()
    {
        return $this->belongsTo('App\Model\Pelanggan','id_pelanggan');
    }

    public function stat()
    {
        return $this->belongsTo('App\Model\StatusBarang','status');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d F Y');
    }
}
