<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class KategoriBarang extends Model
{
    protected $table = 'kategori';
	protected $fillable = ['name'];
    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d F Y');
    }

    public function favorite()
    {
        return $this->hasMany('App\Model\Barang');
    }
}
