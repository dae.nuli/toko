<?php

namespace App\Model;

// use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
class Role extends EntrustRole
{
	protected $fillable = ['name','display_name','description'];

    // public function roleUser()
    // {
    // 	return $this->hasMany('App\Model\RoleUser');
    // }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * many-to-many relationship method.
     *
     * @return QueryBuilder
     */
    // public function permissions()
    // {
    //     return $this->belongsToMany('App\Model\Permission');
    // }

    // public function assign(Permission $permission)
    // {
    //     return $this->permissions()->save($permission);
    // }
}
