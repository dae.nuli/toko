<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Konfirmasi extends Model
{
    protected $table = 'konfirmasi';

	protected $fillable = ['id_transaksi','no_resi'];
    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d F Y');
    }
}
