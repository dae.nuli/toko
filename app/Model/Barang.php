<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Barang extends Model
{
    protected $table = 'barang';
	protected $fillable = ['name','price','description','stock','category','image'];
    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d F Y');
    }

    public function favorite()
    {
        return $this->hasMany('App\Model\Favorite');
    }

    public function transaction()
    {
        return $this->hasMany('App\Model\Transaksi');
    }
}
