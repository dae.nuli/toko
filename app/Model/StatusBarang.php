<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusBarang extends Model
{
    protected $table = 'status_barang';
	protected $fillable = ['name'];
    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d F Y');
    }

    public function transaction()
    {
        return $this->hasMany('App\Model\Transaksi');
    }
}
