<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorite';
    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function barang()
    {
    	return $this->belongsTo('App\Model\Barang');
    }
}
