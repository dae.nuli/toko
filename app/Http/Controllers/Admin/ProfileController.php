<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class ProfileController extends Controller
{
    private $folder   = 'admin.profile';
    private $uri      = 'admin.profile';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(User $tabel)
    {
        $this->tabel = $tabel;
        $this->title = trans('profile.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
		$data['path']     = 'Index';
		$data['title']    = $this->title;
		$data['template'] = $this->template;
		$data['index']    = $this->tabel->findOrFail(auth()->user()->id);
		
		$data['action']   = route($this->uri.'.store');
        return view($this->folder.'.index',$data);
    }

    public function store(Request $request)
    {
    	$this->validatePost($request);

        $request->merge(['password' => bcrypt($request->password)]);
        $this->tabel->findOrFail(auth()->user()->id)->update($request->all());
        // $this->tabel->create($request->all());
        return redirect($this->url)->with('success',trans('message.update'));
    }

    protected function validatePost(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'email'  => 'required|unique:users,email,'.auth()->user()->id,
            'password'  => 'min:8|confirmed'
        ]);
    }
}
