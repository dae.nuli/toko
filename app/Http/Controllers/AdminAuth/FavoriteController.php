<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Favorite;

class FavoriteController extends Controller
{
    private $folder   = 'admin.favorite';
    private $uri      = 'favorite';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(Favorite $tabel)
    {
        $this->tabel = $tabel;
        $this->title = trans('favorite.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        $data['create']    = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','id_user','id_barang']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            ->edit_column('id_user',function($index){
                return $index->user->name;
            })
            ->edit_column('id_barang',function($index){
                return $index->barang->name;
            })
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>';
                $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>".trans("button.show")."</a>";
                $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>";
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }
  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['path']      = 'Detail';
        $data['url']       = $this->url;
        return view($this->folder.'.show',$data);
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $br = $this->tabel->findOrFail($id);
        $br->delete();

        return redirect($this->url)->with('success',trans('message.delete'));
    }

 
    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $type = $this->tabel->findOrFail($value);
                $type->delete();
            }
            return redirect($this->url)->with('success',trans('message.delete.all'));
        }
    }

}
