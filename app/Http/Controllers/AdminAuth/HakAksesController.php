<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
use App\Model\Permission;
use App\Model\PermissionRole;
use Datatables, Form, DB;

class HakAksesController extends Controller
{
    private $folder   = 'admin.access';
    private $uri      = 'admin.access';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(Role $tabel)
    {
        $this->middleware('permission:index_access', ['only' => ['index','getData','show']]);
        $this->middleware('permission:create_access', ['only' => ['create','store']]);
        $this->middleware('permission:edit_access', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_access', ['only' => ['destroy','postDeleteAll']]);

        $this->tabel = $tabel;
        $this->title = trans('access.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        $data['create']    = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','name','display_name','description','created_at']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            // ->edit_column('id_user',function($index){
            //     return $index->user->name;
            // })
            ->edit_column('description',function($index){
                return str_limit($index->description, 30);
            })
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= (auth()->user()->can('edit_access')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>' : '';
                $tag .= (auth()->user()->can('index_access')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>".trans("button.show")."</a>" : '';
                $tag .= (auth()->user()->can('delete_access')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>" : '';
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']    = $this->title;
        $data['template'] = $this->template;
        $data['permission'] = Permission::get();
        $data['method']   = 'POST';
        $data['path']     = 'Create';
        $data['action']   = route($this->uri.'.store');
        $data['url']      = $this->url;
        return view($this->folder.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatePost($request);
// dd($request->all());
        $added = $this->tabel->create([
            'name' => $request->name, 
            'display_name' => $request->display_name, 
            'description' => ! empty($request->description) ? $request->description : NULL
            ]);
        // print_r($request->permission);
        // return;
        if (isset($request->permission)) {
        //    foreach ($request->permission as $key => $value) {
                $added->perms()->sync($request->permission);
           // }
        }
        return redirect($this->url)->with('success',trans('message.create'));
    }

    public function show($id)
    {
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['permissionRole']     = PermissionRole::where('role_id',$id)->get();
        $data['path']      = 'Detail';
        $data['url']       = $this->url;
        return view($this->folder.'.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['title']     = $this->title;
        $data['path']      = 'Edit';
        $data['method']    = 'PUT';
        $data['url']       = $this->url;
        $data['action']    = route($this->uri.'.update',$id);
        $data['permission'] = Permission::get();
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validatePost($request);

        $added = $this->tabel->findOrFail($id)->update([
            'name' => $request->name, 
            'display_name' => $request->display_name, 
            'description' => ! empty($request->description) ? $request->description : NULL
            ]);
        PermissionRole::where('role_id',$id)->delete();
        if (isset($request->permission)) {
            foreach ($request->permission as $key => $value) {
                PermissionRole::create([
                    'permission_id' => $value,
                    'role_id' => $id
                ]);
            }
        }
        // if (isset($request->permission)) {
        //     $added->perms()->sync($request->permission);
        // }

        // $this->tabel->findOrFail($id)->update($request->all());
        return redirect($this->url)->with('success',trans('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $role = Role::findOrFail($id);
        $rUser = DB::table('role_user')->where('role_id',$id)->first();
        if (count($rUser)) {
            return redirect($this->url)->with('error',trans('message.delete.cannot'));
        }
        PermissionRole::where('role_id',$id)->delete();
        DB::table('role_user')->where('role_id',$id)->delete();
        DB::table('roles')->where('id',$id)->delete();
        // $role->perms()->sync([]);
        // $role = $this->tabel->findOrFail($id);
        // $role->delete(); // This will work no matter what

        // // Force Delete
        // $role->users()->sync([]); // Delete relationship data
        // $role->forceDelete();
        return redirect($this->url)->with('success',trans('message.delete'));
    }

 
    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $rUser = DB::table('role_user')->where('role_id',$value)->first();
                if (count($rUser)) {
                    return redirect($this->url)->with('error',trans('message.delete.cannot'));
                }
                PermissionRole::where('role_id',$value)->delete();
                DB::table('role_user')->where('role_id',$value)->delete();
                DB::table('roles')->where('id',$value)->delete();
                // $type = $this->tabel->findOrFail($value);
                // $type->delete();

                // // Force Delete
                // $type->users()->sync([]); // Delete relationship data
                // $type->perms()->sync([]);
            }
            return redirect($this->url)->with('success',trans('message.delete.all'));
        }
    }

    protected function validatePost(Request $request)
    {
        if(isset($request->id)) {
            $this->validate($request, [
                'name'  => 'required|unique:roles,name,'.$request->id,
                'display_name'  => 'required'
            ]);
        }else{
            $this->validate($request, [
                'name'  => 'required|unique:roles,name',
                'display_name'  => 'required'
            ]);
        }
    }
}
