<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\RoleUser;
use Datatables, Form, Helpers, DB, Auth;

class PegawaiController extends Controller
{
    private $folder   = 'admin.staff';
    private $uri      = 'admin.staff';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(User $tabel)
    {
        $this->middleware('permission:index_staff', ['only' => ['index','getData']]);
        $this->middleware('permission:create_staff', ['only' => ['create','store']]);
        $this->middleware('permission:edit_staff', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_staff', ['only' => ['destroy','postDeleteAll']]);
        
        $this->tabel = $tabel;
        $this->title = trans('staff.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        $data['create']    = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','name','email']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            ->add_column('role',function($index) {
                // return isset($index->role->name)?$index->role->name:'-';
                return Helpers::RoleName($index->id);
            })
            ->add_column('created_at',function($index) {
                return $index->created_at;
            })
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= (auth()->user()->can('edit_staff')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>' : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>".trans("button.show")."</a>";
                $tag .= (auth()->user()->can('delete_staff')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>" : '';
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']    = $this->title;
        $data['template'] = $this->template;
        $data['method']   = 'POST';
        $data['path']     = 'Create';
        $data['action']   = route($this->uri.'.store');
        $data['url']      = $this->url;
        $data['roles']    = Role::get();
        return view($this->folder.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatePost($request);

        $request->merge(['password' => bcrypt($request->password)]);
        $add = $this->tabel->create($request->all());

        RoleUser::create(['user_id' => $add->id, 'role_id' => $request->role]);
        return redirect($this->url)->with('success',trans('message.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['path']      = 'Detail';
        $data['url']       = $this->url;
        return view($this->folder.'.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['title']     = $this->title;
        $data['path']      = 'Edit';
        $data['method']    = 'PUT';
        $data['url']       = $this->url;
        $data['action']    = route($this->uri.'.update',$id);
        $data['roles']    = Role::get();
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validatePost($request);
// dd($request->all());
        // $request->merge(['password' => bcrypt($request->password)]);
        if (empty($request->password)){
            unset($request['password']);
        } else {
            $request->merge(['password' => bcrypt($request->password)]);
        }
        $this->tabel->findOrFail($id)->update($request->all());

        // RoleUser::updateOrCreate(['user_id' => $id], ['role_id' => $request->role]);
        $ru = RoleUser::where('user_id',$id)
                    ->first();
        if (count($ru)) {
            DB::table('role_user')
                    ->where('user_id', $id)
                    ->update(['role_id' => $request->role]);
        } else {
            RoleUser::create(['user_id' => $id, 'role_id' => $request->role]);
        }
        return redirect($this->url)->with('success',trans('message.update'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $this->tabel->findOrFail($id)->delete();
        RoleUser::where('user_id',$id)->delete();
        return redirect($this->url)->with('success',trans('message.delete'));
    }

 
    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $type = $this->tabel->findOrFail($value);
                RoleUser::where('user_id',$value)->delete();
                $type->delete();
            }
            return redirect($this->url)->with('success',trans('message.delete.all'));
        }
    }

    protected function validatePost(Request $request)
    {
        if(isset($request->id)) {
            $this->validate($request, [
                'name'  => 'required',
                'email'  => 'required|unique:users,email,'.$request->id,
                'password'  => 'min:8|confirmed'
            ]);
        }else{
            $this->validate($request, [
                'name'  => 'required',
                'email'  => 'required|unique:users,email',
                'password'  => 'required|min:8|confirmed'
            ]);
        }
    }
}
