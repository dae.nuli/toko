<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Konfirmasi;
use App\Model\Transaksi;
use Datatables, Form;
use App\Enum\Permission;

class KonfirmasiController extends Controller
{
    private $folder   = 'admin.confirmation';
    private $uri      = 'admin.confirmation';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(Konfirmasi $tabel)
    {
        $this->middleware('permission:index_confirmation', ['only' => ['index','getData']]);
        $this->middleware('permission:edit_confirmation', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_confirmation', ['only' => ['destroy','postDeleteAll']]);

        $this->tabel = $tabel;
        $this->title = trans('confirmation.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
// $aa = \App\Model\Permission::with('roles')->get();
// dd($aa);
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        $data['create']    = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','id_transaksi','no_resi']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= (auth()->user()->can('edit_confirmation')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>' : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>".trans("button.show")."</a>";
                $tag .= (auth()->user()->can('delete_confirmation')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>" : '';
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }
  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['path']      = 'Detail';
        $data['url']       = $this->url;
        return view($this->folder.'.show',$data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['title']     = $this->title;
        $data['path']      = 'Edit';
        $data['method']    = 'PUT';
        $data['url']       = $this->url;
        $data['action']    = route($this->uri.'.update',$id);
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'resi' => 'required|numeric'
        ]);

        $index = $this->tabel->findOrFail($id);
        $index->no_resi = $request->resi;
        $index->save();
        return redirect($this->url)->with('success',trans('message.update'));
    }

    public function destroy($id)
    {
        $br = $this->tabel->findOrFail($id);
        // return $var = Transaksi::findOrFail($br->id_transaksi);
        $br->delete();

        return redirect($this->url)->with('success',trans('message.delete'));
    }

 
    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $type = $this->tabel->findOrFail($value);
                $type->delete();
            }
            return redirect($this->url)->with('success',trans('message.delete.all'));
        }
    }
}
