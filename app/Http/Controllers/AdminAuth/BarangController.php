<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Barang;
use App\Model\KategoriBarang;
use Datatables, Form, Storage, File;

class BarangController extends Controller
{
    private $folder   = 'admin.item';
    private $uri      = 'admin.item';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(Barang $tabel)
    {
        $this->middleware('permission:index_item', ['only' => ['index','getData']]);
        $this->middleware('permission:create_item', ['only' => ['create','store']]);
        $this->middleware('permission:edit_item', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_item', ['only' => ['destroy','postDeleteAll']]);

        $this->tabel = $tabel;
        $this->title = trans('item.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        $data['create']    = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','name','price','stock','image','created_at']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            ->edit_column('harga','{{number_format($price,0,",",".")}}')
            ->edit_column('image','<img src={{asset("store/".$image)}} width="100" />')
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= (auth()->user()->can('edit_item')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>' : '';
                // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>".trans("button.show")."</a>";
                $tag .= (auth()->user()->can('delete_item')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>" : '';
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']    = $this->title;
        $data['template'] = $this->template;
        $data['category']  = KategoriBarang::orderBy('id','desc')->get();
        $data['method']   = 'POST';
        $data['path']     = 'Create';
        $data['action']   = route($this->uri.'.store');
        $data['url']      = $this->url;
        return view($this->folder.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatePost($request);

        $val = $request->photo;
        if(isset($val)) {
            $directory = base_path('public/store');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);
            
            $request->merge(['image' => $name]);
        }else{
            unset($request['image']);
        }
        $this->tabel->create($request->all());
        return redirect($this->url)->with('success',trans('message.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['path']      = 'Detail';
        $data['url']       = $this->url;
        return view($this->folder.'.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['template']  = $this->template;
        $data['index']     = $this->tabel->findOrFail($id);
        $data['title']     = $this->title;
        $data['category']  = KategoriBarang::orderBy('id','desc')->get();
        $data['path']      = 'Edit';
        $data['method']    = 'PUT';
        $data['url']       = $this->url;
        $data['action']    = route($this->uri.'.update',$id);
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validatePost($request);
        
        $val = $request->photo;
        if(isset($val)) {
            $directory = base_path('public/store');
            $name      = rand(111,9999999).'.'.$val->extension();
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0777);    
            }
            $val->move($directory, $name);

            $item = $this->tabel->findOrFail($id);
            Storage::delete($item->image);
            $request->merge(['image' => $name]);

        }else{
            unset($request['image']);
        }

        $this->tabel->findOrFail($id)->update($request->all());
        return redirect($this->url)->with('success',trans('message.update'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $br = $this->tabel->findOrFail($id);
        Storage::delete($br->image);
        $br->delete();

        return redirect($this->url)->with('success',trans('message.delete'));
    }

 
    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $type = $this->tabel->findOrFail($value);
                Storage::delete($type->image);
                $type->delete();
            }
            return redirect($this->url)->with('success',trans('message.delete.all'));
        }
    }

    protected function validatePost(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|max:100',
            'price'       => 'required|numeric',
            'description' => 'required',
            'stock'       => 'required|numeric',
            'category'    => 'required'
        ]);
    }
}
