<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pelanggan;
use Datatables, Form;

class PelangganController extends Controller
{
    private $folder   = 'admin.customer';
    private $uri      = 'admin.customer';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(Pelanggan $tabel)
    {
        $this->tabel = $tabel;
        $this->title = trans('customer.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        $data['create']    = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','name','email','created_at']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>';
                $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>".trans("button.show")."</a>";
                $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>";
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }
}
