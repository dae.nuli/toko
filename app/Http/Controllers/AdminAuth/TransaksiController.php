<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Transaksi;
use App\Model\Konfirmasi;
use Datatables;
use Form;

class TransaksiController extends Controller
{
    private $folder   = 'admin.transaction';
    private $uri      = 'admin.transaction';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(Transaksi $tabel)
    {
        $this->middleware('permission:index_transaction', ['only' => ['index','getData']]);
        $this->middleware('permission:complete_transaction', ['only' => ['modal','store']]);
        $this->middleware('permission:delete_transaction', ['only' => ['destroy','postDeleteAll']]);

        $this->tabel = $tabel;
        $this->title = trans('transaction.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        $data['url']       = $this->url;

        $data['action']    = route($this->uri.'.delete.all');
        $data['ajax']      = route($this->uri.'.getData');
        return view($this->folder.'.index',$data);
    }

    public function modal($id)
    {
        $data['index']     = Konfirmasi::where('id_transaksi',$id)->first();
        $data['action']    = route($this->uri.'.store',$id);
        return view($this->folder.'.modal',$data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $index = $this->tabel->select(['id','id_pelanggan','id_barang','total','jml_barang','status','is_complete','created_at']);
            return Datatables::of($index)
            ->edit_column('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
            ->edit_column('id_pelanggan',function($index){
                return $index->customer->name;
            })
            ->edit_column('id_barang',function($index){
                return $index->barang->name;
            })
            ->edit_column('status',function($index){
                return $index->stat->name;
            })
            ->edit_column('is_complete',function($index){
                if($index->is_complete) {
                    return '<small class="label label-success">'.trans('transaction.option.complete').'</small>';
                } else {
                    return '<small class="label label-warning">'.trans('transaction.option.uncomplete').'</small>';
                }
            })
            ->add_column('action',function($index) {
                $tag = Form::open(array("url"=>route($this->uri.'.destroy',$index->id),"method"=>"DELETE"));
                $tag .= (auth()->user()->can('complete_transaction')) ? "<a data-url=".route($this->uri.'.modal',$index->id)." class='btn btn-success btn-xs complete' data-id='".$index->id."'>".trans("button.complete")."</a>" : '';
                // $tag .= " <a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>".trans("button.edit").'</a>';
                $tag .= (auth()->user()->can('delete_transaction')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>".trans("button.delete")."</button>" : '';
                $tag .= Form::close();
                return $tag;
            })->make();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        // return $id;
        $this->validatePost($request);

        Konfirmasi::updateOrCreate(['id_transaksi' => $id],['no_resi' => $request->resi]);
        $var = $this->tabel->findOrFail($id);
        $var->is_complete = '1';
        $var->save();
        return redirect($this->url)->with('success',trans('message.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->tabel->findOrFail($id)->delete();
        return redirect($this->url)->with('success',trans('message.delete'));
    }

 
    public function postDeleteAll(Request $request)
    {
        $ID = $request->id;
        if(count($ID)>0){
            foreach ($ID as $key => $value) {
                $type = $this->tabel->findOrFail($value);
                $type->delete();
            }
            return redirect($this->url)->with('success',trans('message.delete.all'));
        }
    }

    protected function validatePost(Request $request)
    {
        $this->validate($request, [
            'resi' => 'required|numeric'
        ]);
    }
}
