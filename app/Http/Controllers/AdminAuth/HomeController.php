<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    private $folder   = 'admin.dashboard';
    private $uri      = 'admin.home';
    private $template = 'admin.layout.content';
    private $title;

    public function __construct()
    {
        // $this->tabel  = $tabel;
        $this->title  = trans('home.header.title');
        $this->url    = route($this->uri);
    }

    public function index()
    {
        $data['path']      = 'Index';
        $data['title']     = $this->title;
        $data['template']  = $this->template;
        return view($this->folder.'.index',$data);
    }
}
