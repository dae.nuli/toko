<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TentangKami;

class AboutController extends Controller
{
    private $folder   = 'admin.about';
    private $uri      = 'admin.about';
    private $template = 'admin.layout.content';
    private $title;
    private $tabel;

    public function __construct(TentangKami $tabel)
    {
        $this->middleware('permission:about', ['only' => ['index','store']]);

        $this->tabel = $tabel;
        $this->title = trans('about.header.title');
        $this->url   = route($this->uri.'.index');
    }

    public function index()
    {
		$data['path']     = 'Index';
		$data['title']    = $this->title;
		$data['template'] = $this->template;
		$data['index']    = $this->tabel->findOrFail(1);
		
		$data['action']   = route($this->uri.'.store');
        return view($this->folder.'.index',$data);
    }

    public function store(Request $request)
    {
        $this->tabel->findOrFail($request->id)->update($request->all());
        // $this->tabel->create($request->all());
        return redirect($this->url)->with('success',trans('message.update'));
    }
}
