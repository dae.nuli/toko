<?php
use App\Model\PermissionRole;
use App\Model\Permission;
use App\Model\Role;
use App\Model\RoleUser;

class Helpers
{
	public static function CheckPermission($permissionId,$roleId)
	{
		$db = PermissionRole::where('permission_id',$permissionId)
		->where('role_id',$roleId)
		->count();
		if ($db) {
			return 'checked=""';
		}
	}
	
	public static function PermissionName($permissionId)
	{
		$db = Permission::find($permissionId);
		if (count($db)) {
			return $db->name;
		}
	}

	public static function RoleName($userId)
	{
		$db = DB::table('role_user')
		->where('user_id',$userId)
		->first();
		if (count($db)) {
			$rols = Role::find($db->role_id);
			return $rols->display_name;
		} else {
			return '-';
		}
	}


	public static function RoleCheck($userId)
	{
		$db = RoleUser::where('user_id',$userId)
		->first();
		if (count($db)) {
			return $db->role_id;
		} else {
			return 0;
		}
	}
}