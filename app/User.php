<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Carbon\Carbon;

// use Illuminate\Database\Eloquent\Model;
class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function favorite()
    {
        return $this->hasMany('App\Model\Favorite');
    }

    public function transaction()
    {
        return $this->hasMany('App\Model\Transaksi');
    }

    // public function role()
    // {
    //     return $this->hasMany('App\Model\RoleUser');
    // }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d F Y');
    }


    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
   
    /**
     * Many-To-Many Relationship Method for accessing the User->roles
     *
     * @return QueryBuilder Object
     */
    // public function roles()
    // {
    //     return $this->belongsToMany('App\Model\Role');
    // }

    // public function hasRole($role)
    // {
    //     if (is_string($role)) {
    //         # code...
    //         return $this->roles->contains('name',$role);
    //     }

    //     return !! $role->intersect($this->roles)->count();

    // }

// public function setFirstNameAttribute($value)
//     {
//         $this->attributes['first_name'] = strtolower($value);
//     }
}
