<?php

use Illuminate\Database\Seeder;
use App\Model\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$role = [
        	[
                'name' => 'index_transaction',
                'display_name' => 'Transaction Display',
                'description' => 'Transaction Display'
            ],
            [
                'name' => 'complete_transaction',
                'display_name' => 'Transaction Complete',
                'description' => 'Transaction Complete'
            ],
            [
                'name' => 'delete_transaction',
                'display_name' => 'Transaction Delete',
                'description' => 'Transaction Delete'
            ],

        	[
        		'name' => 'index_confirmation',
        		'display_name' => 'Confirmation Display',
        		'description' => 'Confirmation Display'
        	],
            [
                'name' => 'edit_confirmation',
                'display_name' => 'Confirmation Edit',
                'description' => 'Confirmation Edit'
            ],
            [
                'name' => 'delete_confirmation',
                'display_name' => 'Confirmation Delete',
                'description' => 'Confirmation Delete'
            ],

            [
                'name' => 'index_category',
                'display_name' => 'Category Display',
                'description' => 'Category Display'
            ],
            [
                'name' => 'create_category',
                'display_name' => 'Category Create',
                'description' => 'Category Create'
            ],
            [
                'name' => 'edit_category',
                'display_name' => 'Category Edit',
                'description' => 'Category Edit'
            ],
            [
                'name' => 'delete_category',
                'display_name' => 'Category Delete',
                'description' => 'Category Delete'
            ],

            [
                'name' => 'index_item',
                'display_name' => 'Item Display',
                'description' => 'Item Display'
            ],
            [
                'name' => 'create_item',
                'display_name' => 'Item Create',
                'description' => 'Item Create'
            ],
            [
                'name' => 'edit_item',
                'display_name' => 'Item Edit',
                'description' => 'Item Edit'
            ],
            [
                'name' => 'delete_item',
                'display_name' => 'Item Delete',
                'description' => 'Item Delete'
            ],

            [
                'name' => 'index_staff',
                'display_name' => 'Staff Display',
                'description' => 'Staff Display'
            ],
            [
                'name' => 'create_staff',
                'display_name' => 'Staff Create',
                'description' => 'Staff Create'
            ],
            [
                'name' => 'edit_staff',
                'display_name' => 'Staff Edit',
                'description' => 'Staff Edit'
            ],
            [
                'name' => 'delete_staff',
                'display_name' => 'Staff Delete',
                'description' => 'Staff Delete'
            ],

            [
                'name' => 'index_bank',
                'display_name' => 'Bank Display',
                'description' => 'Bank Display'
            ],
            [
                'name' => 'create_bank',
                'display_name' => 'Bank Create',
                'description' => 'Bank Create'
            ],
            [
                'name' => 'edit_bank',
                'display_name' => 'Bank Edit',
                'description' => 'Bank Edit'
            ],
            [
                'name' => 'delete_bank',
                'display_name' => 'Bank Delete',
                'description' => 'Bank Delete'
            ],

            [
                'name' => 'index_status',
                'display_name' => 'Status Display',
                'description' => 'Status Display'
            ],
            [
                'name' => 'create_status',
                'display_name' => 'Status Create',
                'description' => 'Status Create'
            ],
            [
                'name' => 'edit_status',
                'display_name' => 'Status Edit',
                'description' => 'Status Edit'
            ],
            [
                'name' => 'delete_status',
                'display_name' => 'Status Delete',
                'description' => 'Status Delete'
            ],
            [
                'name' => 'about',
                'display_name' => 'About',
                'description' => 'About'
            ],

            [
                'name' => 'index_access',
                'display_name' => 'Access Display',
                'description' => 'Access Display'
            ],
            [
                'name' => 'create_access',
                'display_name' => 'Access Create',
                'description' => 'Access Create'
            ],
            [
                'name' => 'edit_access',
                'display_name' => 'Access Edit',
                'description' => 'Access Edit'
            ],
            [
                'name' => 'delete_access',
                'display_name' => 'Access Delete',
                'description' => 'Access Delete'
            ],
            
        ];

        foreach ($role as $key => $value) {
        	Permission::insert($value);
        }
    }
}
