<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tentang_kami')->insert([
            'name' => 'Toko Baju',
            'address' => 'Jl. Cikini',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
