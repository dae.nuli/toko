<?php

use Illuminate\Database\Seeder;
use App\Model\Role;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$role = [
        	[
        		'name' => 'admin',
        		'display_name' => 'Administrator',
        		'description' => 'Administrator Description',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'name' => 'staff',
        		'display_name' => 'Staff',
        		'description' => 'Staff Description',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	]
        ];

        foreach ($role as $key => $value) {
        	Role::insert($value);
        }
    }
}
