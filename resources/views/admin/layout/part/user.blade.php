<li class="dropdown user user-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"/>
    <span class="hidden-xs">{{auth()->guard('admin')->user()->name}}</span>
  </a>
  <ul class="dropdown-menu">
    <!-- User image -->
    <li class="user-header">
      <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
      <p>
        {{Str::words(auth()->guard('admin')->user()->name,2,'')}}
        <small>{{trans('message.member.since')}} {{date('M. Y',strtotime(auth()->guard('admin')->user()->created_at))}}</small>
      </p>
    </li>
    <!-- Menu Body -->
{{--     <li class="user-body">
      <div class="col-xs-4 text-center">
        <a href="#">Followers</a>
      </div>
      <div class="col-xs-4 text-center">
        <a href="#">Sales</a>
      </div>
      <div class="col-xs-4 text-center">
        <a href="#">Friends</a>
      </div>
    </li> --}}
    <!-- Menu Footer-->
    <li class="user-footer">
      <div class="pull-left">
        <a href="{{url('admin/profile')}}" class="btn btn-default btn-flat">{{trans('message.profile')}}</a>
      </div>
      <div class="pull-right">
        <a href="{{ url('/admin/logout') }}" class="btn btn-default btn-flat"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            {{trans('message.logout')}}
        </a>

        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      </div>
    </li>
  </ul>
</li>