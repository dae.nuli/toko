<?php 
$uri = (request()->segment(1)=='en')?request()->segment(3):request()->segment(2);
?>
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li {{($uri=='dashboard')?"class=active":''}}>
    <a href="{{ route('admin.home') }}">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
  </li>
  
  @if(auth()->user()->can('index_transaction'))
  <li {{($uri=='transaction')? "class=active" :''}}>
    <a href="{{ route('admin.transaction.index') }}">
      <i class="fa fa-shopping-cart"></i> <span>{{trans('menu.transaction')}}</span>
    </a>
  </li>
  @endif

  @if(auth()->user()->can('index_confirmation'))
  <li {{($uri=='confirmation')? "class=active" :''}}>
    <a href="{{ route('admin.confirmation.index') }}">
      <i class="fa fa-check-circle"></i> <span>{{trans('menu.confirmation')}}</span>
    </a>
  </li>
  @endif
  
  @if(auth()->user()->can('index_category'))
  <li {{($uri=='category')?'class=active':''}}>
    <a href="{{ route('admin.category.index') }}">
      <i class="fa fa-briefcase"></i> <span>{{trans('menu.category')}}</span>
    </a>
  </li>
  @endif

  @if(auth()->user()->can('index_item'))
  <li {{($uri=='item')?'class=active':''}}>
    <a href="{{ route('admin.item.index') }}">
      <i class="fa fa-briefcase"></i> <span>{{trans('menu.item')}}</span>
    </a>
  </li>
  @endif
  
  @if(auth()->user()->can('index_staff'))
  <li {{($uri=='staff')?'class=active':''}}>
    <a href="{{ route('admin.staff.index') }}">
      <i class="fa fa-users"></i> <span>{{trans('menu.staff')}}</span>
    </a>
  </li>
  @endif
  
  @if(auth()->user()->can('index_favorite'))
  <li {{($uri=='favorite')?'class=active':''}}>
    <a href="">
      <i class="fa fa-star"></i> <span>{{trans('menu.favorite')}}</span>
    </a>
  </li>
  @endif
  
  <li class="treeview {{($uri=='bank'||$uri=='itemStatus'||$uri=='about'||$uri=='access')?'active':''}}">
    <a href="#">
      <i class="fa fa-gear"></i> <span>{{trans('menu.setting')}}</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu menu-open">
      @if(auth()->user()->can('index_bank'))
      <li {{($uri=='bank')?'class=active':''}}><a href="{{ route('admin.bank.index') }}"><i class="fa fa-circle-o"></i> {{trans('menu.bank')}}</a></li>
      @endif
      {{-- <li {{($uri=='status')?'class=active':''}}><a href="{{ route('admin.status.index') }}"><i class="fa fa-circle-o"></i> {{trans('menu.status')}}</a></li> --}}

      @if(auth()->user()->can('index_status'))
      <li {{($uri=='itemStatus')?'class=active':''}}><a href="{{ route('admin.itemStatus.index') }}"><i class="fa fa-circle-o"></i> {{trans('menu.status.item')}}</a></li>
      @endif

      @if(auth()->user()->can('about'))
      <li {{($uri=='about')?'class=active':''}}><a href="{{ route('admin.about.index') }}"><i class="fa fa-circle-o"></i> {{trans('menu.about')}}</a></li>
      @endif

      @if(auth()->user()->can('index_access'))
      <li {{($uri=='access')?'class=active':''}}><a href="{{ route('admin.access.index') }}"><i class="fa fa-circle-o"></i> {{trans('menu.access')}}</a></li>
      @endif
    </ul>
  </li>
{{--   <li>
    <a href="pages/mailbox/mailbox.html">
      <i class="fa fa-envelope"></i> <span>Mailbox</span>
      <small class="label pull-right bg-yellow">12</small>
    </a>
  </li> --}}
{{--   <li class="header">LABELS</li>
  <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
  <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
  <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
</ul>