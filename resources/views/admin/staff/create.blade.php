@extends($template)

@section('end-script')
  @parent
  <!-- Select2 -->
  <script src="{{asset('adminlte/dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/security.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      modules : 'security',
      onSuccess : function() {
        waiting();
      }
  });
  </script>
@stop

@section('body-content')
@if(count($errors))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal'))}}
      @if(isset($index))
      <input type="hidden" name="id" value="{{$index->id}}">
      @endif
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('staff.table.name')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="name" value="{{isset($index)?$index->name:old('name')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('staff.validation.name')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('staff.table.email')}}</label>
          <div class="col-sm-8">
            <input type="email" class="form-control" name="email" value="{{isset($index)?$index->email:old('email')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('staff.validation.email')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('staff.table.role')}}</label>
          <div class="col-sm-8">
            <select class="form-control" name="role" data-validation="required" data-validation-error-msg="{{trans('staff.validation.role')}}.">
              <option value>- {{trans('custom.option.default')}} -</option>
              @foreach($roles as $row)
                @if(isset($index))
                  <option value="{{$row->id}}" {{($row->id==Helpers::RoleCheck($index->id)) ? 'selected':'' }}>{{$row->display_name}}</option>
                @else
                  <option value="{{$row->id}}">{{$row->display_name}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('staff.table.password')}}</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" name="password_confirmation" autocomplete="off" {!!isset($index)?'data-validation-optional="true"':''!!} data-validation="length" data-validation-length="min8" data-validation-error-msg="{{trans('staff.validation.password')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('staff.table.password.confirmation')}}</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" name="password" autocomplete="off" data-validation="confirmation" data-validation-error-msg="{{trans('staff.validation.password.confirmation')}}.">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <a href="{{$url}}" type="submit" class="btn btn-default">{{trans('button.cancel')}}</a>
          <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop