@extends($template)

@section('end-script')
  @parent
  <!-- Select2 -->
  <script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
  <script src="{{asset('dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('dist/js/jquery.price_format.2.0.min.js')}}"></script>
  <script src="{{asset('dist/js/custom.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      onSuccess : function() {
        waiting();
      }
  });
  $('.price').priceFormat({
      prefix:'',
      thousandsSeparator: '.',
      centsLimit: 0
  });
  $(".select2").select2();
  </script>
@stop

@section('head-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
  @parent
@stop

@section('body-content')
@if($errors->has())
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$action,'method'=>'POST','class'=>'form-horizontal','files'=>true))}}
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">Item Category</label>
          <div class="col-sm-8">
            <select class="form-control select2" name="category">
              @foreach($category as $row)
                @if(isset($index))
                  <option value="{{$row->id}}" {{($index->category_id==$row->id)?'selected':''}}>{{$row->name}}</option>
                @else
                  <option value="{{$row->id}}">{{$row->name}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Name</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="name" value="{{isset($index)?$index->name:''}}" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Price</label>
          <div class="col-sm-8">
            <input type="text" class="form-control price" name="price" value="{{isset($index)?$index->price:''}}" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Picture</label>
          <div class="col-sm-8">
            <div class="btn btn-default btn-file">
              <i class="fa fa-paperclip"></i> Attachment
              <input type="file" name="picture">
            </div>
            <p class="help-block">Max. 5MB</p>
            @if(isset($index))
              <img src="{{asset('store/item/'.$index->picture)}}" class="img-responsive">
            @endif
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Description</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="description">{{isset($index)?$index->description:''}}</textarea>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <a href="{{URL::to($url)}}" type="submit" class="btn btn-default">Cancel</a>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop