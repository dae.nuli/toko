@extends($template)

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to($url.'/create')}}" class="btn btn-primary waiting"><i class="fa fa-fw fa-plus"></i> Create</a>
</div>
@stop

@section('head-script')
  @parent
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
@stop

@section('end-script')
  @parent
  <!-- DataTables -->
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('dist/js/custom.js')}}"></script>
  <script>
    $("#example1").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": '{{URL::to($url.'/data')}}',
        "order": [[5,'desc']],
        "columnDefs": [ { //this prevents errors if the data is null
            "targets": "_all",
            "defaultContent": ""
        },{ orderable: false, targets: [0,3,4,6] } ]
    });
  </script>
@stop

@section('body-content')
@if(Session::has('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{Session::get('success')}}.
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{Session::get('error')}}.
</div>
@endif

<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$url.'/delete-all','method'=>'POST','class'=>'confirm'))}}
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="20"><input type="checkbox" id="select_all"></th>
          <th>Name</th>
          <th>Price</th>
          <th>Picture</th>
          <th>Stock (is available)</th>
          <th>Date</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <div class="box-footer clearfix">
       <button type="submit" class="delete-all btn btn-danger">Delete</button>
    </div>
    {{Form::close()}}
  </div>
</div>
</div>
@endsection