@extends($template)

@section('end-script')
  @parent
  <!-- Select2 -->
  <script src="{{asset('adminlte/dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script src="{{asset('js/vue.min.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      onSuccess : function() {
        waiting();
      }
  });

  var app  = new Vue({
    el: "#app",
    data: {
      rows: [
        {name: "", value: ""}
      ]
    },
    methods:{
      addRow: function(){
        this.rows.push({name:"",value:""});
      },
      removeRow: function(row){
        console.log(row);
        this.rows.$remove(row);
      }
    }
  });
  </script>
@stop

@section('body-content')
@if(count($errors))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  {{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal'))}}
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.input.subvaluation')}}</label>
          <div class="col-sm-8">
            <select class="form-control" name="sub_valuation" data-validation="required" data-validation-error-msg="{{trans('item.validation.subvaluation')}}.">
              <option value>- {{trans('custom.input.option.default')}} -</option>
              @foreach($subvaluation as $row)
                @if(isset($index))
                  <option value="{{$row->id}}" {{($index->valuation_id==$row->id)?'selected':''}}>{{$row->name}}</option>
                @else
                  @if(old('sub_valuation'))
                    <option value="{{$row->id}}" {{(old('sub_valuation')==$row->id)?'selected':''}}>{{$row->name}}</option>
                  @else
                    <option value="{{$row->id}}">{{$row->name}}</option>
                  @endif
                @endif
              @endforeach
            </select>
          </div>
        </div>
      </div>
  </div>

  <div class="box box-solid">
      <div class="box-body table-responsive no-padding" id="app">
             <table class="table">
                <thead>
                    <tr>
                        <th>{{trans('item.input.name')}}</th>
                        <th>{{trans('item.input.value')}}</th>
                        <th><a class="btn btn-xs btn-primary" @click="addRow"><i class="fa fa-fw fa-plus"></i> {{trans('button.insertRow')}}</a></th>
                    </tr>
                </thead>
                <tbody class="add-dinamic">
                    <tr v-for="row in rows">
                      <td><input type="text" v-model="row.name" class="form-control" name="name[]" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('item.validation.name')}}."></td>
                      <td><input type="text" v-model="row.value" class="form-control" name="value[]" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('item.validation.value')}}."></td>
                      <td><a @click="removeRow(row)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o"></i> {{trans("button.deleteRow")}}</a></td>
                    </tr>
                </tbody>
            </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer -->
  </div>
  <div class="box-footer">
    <div>
      <a href="{{$url}}" type="submit" class="btn btn-default">{{trans('button.cancel')}}</a>
      <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
    </div>
  </div>
  {{Form::close()}}
</div>
</div>
@stop