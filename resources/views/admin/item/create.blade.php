@extends($template)

@section('end-script')
  @parent
  <!-- Select2 -->
  <script src="{{asset('adminlte/dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      onSuccess : function() {
        waiting();
      }
  });
  </script>
@stop

@section('body-content')
@if(count($errors))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal','files' => true))}}
      @if(isset($index))
      <input type="hidden" name="id" value="{{$index->id}}">
      @endif
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.table.name')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="name" value="{{isset($index)?$index->name:old('name')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('item.validation.name')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.table.category')}}</label>
          <div class="col-sm-8">
            <select class="form-control" name="category" data-validation="required" data-validation-error-msg="{{trans('item.validation.category')}}.">
              <option value>- {{trans('custom.option.default')}} -</option>
              @foreach($category as $row)
                @if(isset($index))
                  <option value="{{$row->id}}" {{($index->category==$row->id)?'selected':''}}>{{$row->name}}</option>
                @else
                  <option value="{{$row->id}}">{{$row->name}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.table.price')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="price" value="{{isset($index)?$index->price:old('price')}}" autocomplete="off" data-validation="number" data-validation-error-msg="{{trans('item.validation.price')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.table.stock')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="stock" value="{{isset($index)?$index->stock:old('stock')}}" autocomplete="off" data-validation="number" data-validation-error-msg="{{trans('item.validation.stock')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.table.image')}}</label>
          <div class="col-sm-8">
            <input type="file" name="photo" autocomplete="off"><br>
            @if(isset($index->image))
              <img src="{{asset('store/'.$index->image)}}" width="300">
            @endif
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('item.table.description')}}</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="description" data-validation="required" data-validation-error-msg="{{trans('item.validation.description')}}.">{{isset($index)?$index->description:old('description')}}</textarea>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <a href="{{$url}}" type="submit" class="btn btn-default">{{trans('button.cancel')}}</a>
          <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop