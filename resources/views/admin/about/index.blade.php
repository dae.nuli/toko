@extends($template)

@section('end-script')
  @parent
  <!-- Select2 -->
  <script src="{{asset('adminlte/dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      onSuccess : function() {
        waiting();
      }
  });
  </script>
@stop

@section('body-content')
@if(count($errors))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{session()->get('success')}}.
</div>
@endif

<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$action,'method'=>'POST','class'=>'form-horizontal'))}}
      @if(isset($index))
      <input type="hidden" name="id" value="{{$index->id}}">
      @endif
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('about.table.name')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="name" value="{{isset($index)?$index->name:old('name')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('about.validation.name')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('about.table.address')}}</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="address" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('about.validation.address')}}.">{{isset($index)?$index->address:old('address')}}</textarea>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop