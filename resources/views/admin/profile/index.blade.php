@extends($template)

@section('end-script')
  @parent
  <!-- Select2 -->
  <script src="{{asset('adminlte/dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      onSuccess : function() {
        waiting();
      }
  });
  </script>
@stop

@section('body-content')
@if(count($errors))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{session()->get('success')}}.
</div>
@endif

<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$action,'method'=>'POST','class'=>'form-horizontal'))}}
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('profile.table.name')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="name" value="{{isset($index)?$index->name:old('name')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('profile.validation.name')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('profile.table.email')}}</label>
          <div class="col-sm-8">
            <input type="email" class="form-control" name="email" value="{{isset($index)?$index->email:old('email')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('profile.validation.email')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('profile.table.password')}}</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" name="password_confirmation" autocomplete="off" {!!isset($index)?'data-validation-optional="true"':''!!} data-validation="length" data-validation-length="min8" data-validation-error-msg="{{trans('profile.validation.password')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('profile.table.password.confirmation')}}</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" name="password" autocomplete="off" data-validation="confirmation" data-validation-error-msg="{{trans('profile.validation.password.confirmation')}}.">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
@role('admin')
    <p>This is visible to users with the admin role. Gets translated to 
    \Entrust::role('admin')</p>
@endrole

@permission('manage-admins')
    <p>This is visible to users with the given permissions. Gets translated to 
    \Entrust::can('manage-admins'). The @can directive is already taken by core 
    laravel authorization package, hence the @permission directive instead.</p>
@endpermission

@ability('admin,owner', 'create-post,edit-user')
    <p>This is visible to users with the given abilities. Gets translated to 
    \Entrust::ability('admin,owner', 'create-post,edit-user')</p>
@endability
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop