@extends($template)

@section('header-content')
@if(auth()->user()->can('create_category'))
<div class="pull-right" style="margin-left:5px">
    <a href="{{$create}}" class="btn btn-primary waiting"><i class="fa fa-fw fa-plus"></i> {{trans('button.create')}}</a>
</div>
@endif
@stop

@section('head-script')
  @parent
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
@stop

@section('end-script')
  @parent
  <!-- DataTables -->
  <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script>
    $("#example1").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": '{{$ajax}}',
        "order": [[2,'desc']],
        "columnDefs": [ { //this prevents errors if the data is null
            "targets": "_all",
            "defaultContent": ""
        },{ orderable: false, targets: [0,3] } ]
    });
  </script>
@stop

@section('body-content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{session()->get('success')}}.
</div>
@endif

@if(session()->has('error'))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {{session()->get('error')}}.
</div>
@endif

<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {!! Form::open(array('url'=>$action,'method'=>'POST','class'=>'confirm')) !!}
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="20"><input type="checkbox" id="select_all"></th>
          <th>{{trans('category.table.name')}}</th>
          <th>{{trans('custom.date')}}</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    @if(auth()->user()->can('delete_category'))
    <div class="box-footer clearfix">
       <button type="submit" class="delete-all btn btn-danger">{{trans('button.delete')}}</button>
    </div>
    @endif
    {!! Form::close() !!}
  </div>
</div>
</div>
@endsection