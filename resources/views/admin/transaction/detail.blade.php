@extends($template)

@section('body-content')

<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div class="box-body form-horizontal">
      <div class="form-group">
        <label class="col-sm-2 control-label">{{trans('transaction.table.user')}}</label>
        <div class="col-sm-8">
          <div class="form-control no-border">{{$index->customer->name}}</div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">{{trans('transaction.table.item')}}</label>
        <div class="col-sm-8">
          <div class="form-control no-border">{{$index->barang->name}}</div>
        </div>
      </div>
    </div>
    <div class="box-footer clearfix">
      <a href="{{$url}}" type="submit" class="btn btn-default">{{trans('button.back')}}</a>
    </div>
  </div>
</div>
</div>
@endsection