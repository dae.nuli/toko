<script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$.validate({
    form : '#requests',
    onSuccess : function() {
        waiting();
    }
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {!!Form::open(array('url'=>$action, 'method'=>'POST','id'=>'requests'))!!}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">{{trans('transaction.table.confirmation')}}</h4>
              </div>
              <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>{{trans('transaction.table.resi')}}</label>
                                <input type="text" class="form-control" autocomplete="off" value="{{isset($index->no_resi)?$index->no_resi:''}}" name="resi" data-validation="number" data-validation-error-msg="{{trans('transaction.validation.resi')}}.">
                            </div>
                        </div>
                    </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-default pull-left" data-dismiss="modal">{{trans('button.cancel')}}</button>
                <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
              </div>
            </div>
        {!!Form::close()!!}
    </div>
</div>