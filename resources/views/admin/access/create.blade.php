@extends($template)

@section('end-script')
  @parent
  <style type="text/css">
  .checkbox {
    display: inline-block;
    padding-right: 10px;
  }
  </style>
  <!-- Select2 -->
  <script src="{{asset('adminlte/dist/js/jquery.blockUI.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/jquery.form-validator.min.js')}}"></script>
  <script src="{{asset('adminlte/dist/js/custom.js')}}"></script>
  <script type="text/javascript">
  $.validate({
      form : '.form-horizontal',
      onSuccess : function() {
        waiting();
      }
  });
  </script>
@stop

@section('body-content')
@if(count($errors))
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('url'=>$action,'method'=>$method,'class'=>'form-horizontal'))}}
      @if(isset($index))
      <input type="hidden" name="id" value="{{$index->id}}">
      @endif
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.name')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="name" value="{{isset($index)?$index->name:old('name')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('access.validation.name')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.display_name')}}</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" name="display_name" value="{{isset($index)?$index->display_name:old('display_name')}}" autocomplete="off" data-validation="required" data-validation-error-msg="{{trans('access.validation.display_name')}}.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.description')}}</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="description" rows="5" autocomplete="off">{{isset($index)?$index->description:old('description')}}</textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.permission')}}</label>
          <div class="col-sm-8">
            @foreach($permission as $i => $row)
            <div class="checkbox">
              <label>
                <input type="checkbox" name="permission[{{$i}}]" value="{{$row->id}}" {{ isset($index) ? Helpers::CheckPermission($row->id,$index->id) : ''}} >
                {{$row->display_name}}
              </label>
            </div>
            @endforeach
            {{-- <input type="text" class="form-control" name="description" value="{{isset($index)?$index->description:old('description')}}" autocomplete="off" data-validation-error-msg="{{trans('access.validation.description')}}."> --}}
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <a href="{{$url}}" type="submit" class="btn btn-default">{{trans('button.cancel')}}</a>
          <button type="submit" class="btn btn-primary">{{trans('button.submit')}}</button>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop