@extends($template)

@section('body-content')

<div class="row">
<div class="col-md-12">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{$path}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::open(array('class'=>'form-horizontal'))}}
      <div class="box-body">
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.name')}}</label>
          <div class="col-sm-8">
            <input class="form-control" value="{{$index->name}}" disabled="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.display_name')}}</label>
          <div class="col-sm-8">
            <input class="form-control" value="{{$index->display_name}}" disabled="">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.description')}}</label>
          <div class="col-sm-8">
            <textarea class="form-control" disabled="" rows="5">{{$index->description}}</textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">{{trans('access.table.permission')}}</label>
          <div class="col-sm-8">
            @foreach($permissionRole as $row)
              <small class="label label-default">{{Helpers::PermissionName($row->permission_id)}}</small>
            @endforeach
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-sm-8 col-sm-offset-2">
          <a href="{{$url}}" type="submit" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
      </div>
      <!-- /.box-footer -->
    {{Form::close()}}
  </div>
</div>
</div>
@stop