<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'header.title'       => 'Access',
    'header.create'      => 'Create',
    
    'table.name'         => 'Name',
    'table.display_name'      => 'Display Name',
    'table.description'      => 'Description',
    'table.permission'      => 'Permission',

    
    'validation.name'    => 'The name field is required',
    'validation.account' => 'The account number must be a number',
    'validation.bank'    => 'The bank field is required',

];
