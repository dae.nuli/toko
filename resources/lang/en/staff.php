<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'header.title'                     => 'Staff',
    'header.create'                    => 'Create',
    
    'table.name'                       => 'Name',
    'table.email'                      => 'Email',
    'table.role'                      => 'Role',
    
    'table.password'                   => 'Password',
    'table.password.confirmation'      => 'Password Confirmation',
    
    'validation.name'                  => 'The name field is required',
    'validation.email'                 => 'The email field is required',
    'validation.role'                 => 'The role field is required',
    'validation.password'              => 'The password must be at least 8',
    'validation.password.confirmation' => 'The password confirmation does not match',

];
