<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'header.title'          => 'Transaction',
    'header.create'         => 'Create',
    
    'table.user'            => 'Customer',
    'table.item'            => 'Item',
    'table.total'           => 'Total',
    'table.total.item'      => 'Total Item',
    'table.status.item'     => 'Status Item',
    'table.status'          => 'Status',
    'table.resi'            => 'Resi Number',
    'table.confirmation'    => 'Confirmation',
    
    'option.complete'       => 'Delivered',
    'option.uncomplete'     => 'Pending',
    
    'validation.user'       => 'The user field is required',
    'validation.item'       => 'The item field is required',
    'validation.total'      => 'The total field is required',
    'validation.total.item' => 'The total item field is required',
    'validation.status'     => 'The status field is required',
    'validation.resi'       => 'The resi must be a number',

];
