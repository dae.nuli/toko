<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'setting'      => 'Setting',
    'transaction'  => 'Transaction',
    'confirmation' => 'Confirmation',
    'item'         => 'Item',
    'staff'        => 'Staff',
    'favorite'     => 'Favorite',
    'bank'         => 'Bank',
    'status.item'  => 'Item Status',
    'status'       => 'Status',
    'category'     => 'Category',
    'about'        => 'About',
    'access'       => 'Access',
];
