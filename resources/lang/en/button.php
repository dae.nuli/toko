<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'complete'  => 'Complete',
    'download'  => 'Download',
    'back'      => 'Back',
    'show'      => 'View',
    'create'    => 'Create',
    'edit'      => 'Edit',
    'delete'    => 'Delete',
    'cancel'    => 'Cancel',
    'submit'    => 'Submit',
    'insertRow' => 'Add Row',
    'deleteRow' => 'Remove Row',

];
