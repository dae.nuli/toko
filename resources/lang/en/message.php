<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'skpd.result'   => 'Skpd has been report',
    'logout'        => 'Logout',
    'profile'       => 'Profile',
    'member.since'  => 'Member since',
    'update'        => 'Data has been updated',
    'create'        => 'Data has been created',
    'delete'        => 'Data has been deleted',
    'delete.cannot' => 'Data is used',
    'delete.all'    => 'Some data has been deleted',
    'answer'        => 'Thanks for answer the question',
    'delete.file'   => 'File has been deleted',
];
