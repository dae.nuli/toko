<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'header.title'       => 'About',
    'header.create'      => 'Update',
    
    'table.name'         => 'Name',
    'table.address'      => 'Address',
    
    'validation.name'    => 'The name field is required',
    'validation.address' => 'The address field is required',

];
