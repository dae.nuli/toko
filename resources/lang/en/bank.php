<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'header.title'       => 'Bank',
    'header.create'      => 'Create',
    
    'table.name'         => 'Name',
    'table.account'      => 'Account Number',
    'table.bank'         => 'Bank',
    
    'input.name'         => 'Name',
    'input.account'      => 'Account Number',
    'input.bank'         => 'Bank',
    
    'validation.name'    => 'The name field is required',
    'validation.account' => 'The account number must be a number',
    'validation.bank'    => 'The bank field is required',

];
