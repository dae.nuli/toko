<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'header.title'       => 'Tentang Kami',
    'header.create'      => 'Update',
    
    'table.name'         => 'Nama',
    'table.address'      => 'Alamat',
    
    
    'validation.name'    => 'Form nama harus diisi',
    'validation.address' => 'Form Alamat harus diisi',

];
