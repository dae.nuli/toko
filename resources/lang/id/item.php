<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'header.title'       => 'Item',
    'header.create'      => 'Create',
    
    'table.name'        => 'Name',
    'table.price'       => 'Price',
    'table.description' => 'Description',
    'table.stock'       => 'Stock',
    'table.category'    => 'Category',
    'table.image'       => 'Image',

    'validation.name'        => 'The name field is required',
    'validation.price'       => 'The price field is required',
    'validation.description' => 'The description field is required',
    'validation.stock'       => 'The stock number must be a number',
    'validation.image'       => 'The image field is required',
    'validation.category'    => 'The category field is required',

    'status.header.title'    => 'Item Status',
    
    'status.table.name'      => 'Name',
    
    'status.validation.name' => 'The name field is required',
];
