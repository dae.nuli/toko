<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'skpd.result'   => 'Skpd telah dinilai',
    'logout'        => 'Keluar',
    'profile'       => 'Profil',
    'member.since'  => 'Member Sejak',
    'update'        => 'Data has been updated',
    'create'        => 'Data telah ditambah',
    'delete'        => 'Data telah dihapus',
    'delete.cannot' => 'Data tidak bisa dihapus',
    'delete.all'    => 'Beberapa data telah dihapus',
    'answer'        => 'Terimakasih atas jawabannya',
    'delete.file'   => 'File telah dihapus',
];
