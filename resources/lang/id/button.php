<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'complete'  => 'Complete',
    'download'  => 'Unduh',
    'back'      => 'Kembali',
    'show'      => 'Lihat',
    'create'    => 'Tambah',
    'edit'      => 'Ubah',
    'delete'    => 'Hapus',
    'cancel'    => 'Batal',
    'submit'    => 'Simpan',
    'insertRow' => 'Tambah Row',
    'deleteRow' => 'Hapus Row',
];
