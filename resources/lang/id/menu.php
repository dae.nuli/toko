<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'setting'      => 'Pengaturan',
    'transaction'  => 'Transaksi',
    'confirmation' => 'Konfirmasi',
    'item'         => 'Barang',
    'staff'        => 'Pegawai',
    'favorite'     => 'Favorit',
    'bank'         => 'Bank',
    'status.item'  => 'Status Barang',
    'status'       => 'Status',
    'category'     => 'Kategori',
    'about'        => 'Tentang Kami',
    'access'       => 'Hak Akses',
];
